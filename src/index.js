import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import 'antd/dist/antd.css';
import "./css/index.css"
import Routes from "./router/index";

ReactDOM.render(
  //  暂时先禁用react的严格模式
  // <React.StrictMode>
    <Routes />,
  // </React.StrictMode>,
  document.getElementById('root')
);

serviceWorker.unregister();
